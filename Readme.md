# Projet Link Invest

<p align="left">
  <img height="400px" width="500px" style="text-align: center;"
   src="https://www.cherche-chantier.fr/wp-content/uploads/2019/09/cr%C3%A9dit-professionnel-2.jpg">
</p>

Objectif : mettre en relation avec des investisseur et outils de tontine à distances

## Sommaire

- [Emploi du temps](#edt)
- [Description Rapide](#desc)
- [Developpeurs](#developpeur)
- [Admnistration](#admnistration)
- [Installation des dépendances](#library)
- [Liens utiles](#links)
    * [authentification](#authentification)
    * [Tuto ionic ](#ionic)
    * [traduction](#traduction)
- [Firebase](#firebase)
    * [Installation](#firebase_install)
    * [Project Account](#firebase_account)

---
<a name="edt"></a>

## (1) Emploi du temps

| Mardi | Jeudi | Samedi | Dimanche |
|:---:|:----:|:---:|:---:|
| 18H | 18H | 18H | 18H |

---

<a name="desc"></a>

## (2) Description Rapide:

- Développement sur le Framework: <strong>IONIC
- Langage: <strong>Typescript
- Deadline: <strong>Unknown

---
<a name="developpeur"></a>

## (3) Developpeur

- Sthorn Bemouela:![](https://media-exp1.licdn.com/dms/image/D4E35AQEBnf-KZMHvDw/profile-framedphoto-shrink_200_200/0/1632732044761?e=1633075200&v=beta&t=RQ7iz7p88zdiqIHi4XlladauSSf8BSt1wztdeqJZVzU)
    * mail: [bemouela@gmail.com]()

---

<a name="#admnistration"></a>

## (4) Admnistration


---
<a name="library"></a>

## (5) Installation des dépendances:

- ```npm install --save @ionic/storage```
- ```npm i @ionic/storage-angular```
- ```npm i @angular/core```
- ```npm install --save @ionic/angular-storage```

---
<a name="links"></a>

## (6) Liens utiles

<a name="authentification"></a>

### - 1)  Authentification

- [authentification linkedin](https://ionicframework.com/docs/v3/native/linkedin/)
- [authentification fcbk](https://drissas.com/ionic-facebook/)
- authentification google
    * [tuto 1](https://edigleyssonsilva.medium.com/adding-google-sign-in-to-your-ionic-3-4-5-app-8ed81744e8ba)
    * [tuto 2](https://ionicthemes.com/tutorials/about/ionic-google-login)
    * [tuto 3](https://enappd.com/blog/implement-google-login-in-ionic-apps-using-firebase/147/)
- [authentification tweter](https://ionicframework.com/docs/native/twitter-connect)

---
<a name="ionic"></a>

### - 2) Tuto ionic

- [css](https://ionicframework.com/docs/layout/css-utilities)
- [animation css](https://animista.net/play/basic/rotate)
  <a name="traduction"></a>

### - 3) traduction

- installer<br>: ```npm install --save google-translate-api```
- [voir tuto](https://file-translate.com/en/app/json-translate)
- [i18n](https://ionicframework.com/docs/v3/developer-resources/ng2-translate/)

---

<a name="firebase"></a>

## (7) Firebase

<a name="firebase_install"></a>

### - 1) Installation:

- ```npm install firebase @angular/fire --save ```
- ```npm install firebase @angular/fire --save ```
- ```npm install cordova-plugin-firebase```
- ```npm install @ionic-native/firebase```


<a name="firebase_account"></a>
### - 2) Project Account:
