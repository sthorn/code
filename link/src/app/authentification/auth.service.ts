import { Injectable } from '@angular/core';
import { AngularFireAuth} from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import firebase from 'firebase/app';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  user: any;


  constructor(
    private firebaseAuth: AngularFireAuth,
    public firestore: AngularFirestore,
    public router:Router
    ){
      this.user = firebaseAuth.authState
        .subscribe(
          user => {
            if (user) {
                this.router.navigateByUrl("acceuil");
            } else {
              this.router.navigateByUrl("home");
            }
          },
          () => {
            this.router.navigateByUrl("home");
          }
        );
        
  }


  mail_login(email: string, password: string) {
    this.firebaseAuth
      .signInWithEmailAndPassword(email, password)
      .then(value => {
        console.log('Nice, it worked!');
      })
      .catch(err => {
        console.log('Something went wrong:',err.message);
      });
  }
  //Deconnexion
  logout() {
    return this.firebaseAuth.signOut();
  }
  //tweter auth
  tweter_login(){
    this.firebaseAuth.signInWithPopup(new firebase.auth.TwitterAuthProvider());
  }
  
  facebook_login(){
    this.firebaseAuth.signInWithPopup(new firebase.auth.FacebookAuthProvider());
  }

  // google authentification
  google_singin(){
    this.firebaseAuth.signInWithPopup(new firebase.auth.GoogleAuthProvider());
  }

  async mail_signup(  user_name:string, user_firstname:string,user_mail:string,
                      user_phone:string,user_password:string):
     Promise<firebase.auth.UserCredential> {
    try {
      const newUserCredential: firebase.auth.UserCredential = await this.firebaseAuth.createUserWithEmailAndPassword(
        user_mail,
        user_password
      );
      await this.firestore
        .doc('userProfile/${newUserCredential.user.uid}')
        .set({ 
          email:user_mail,
          displayName:user_name + " " +user_firstname,
          photoURL:"",
          phoneNumber:user_phone
        });

      /*
        Here we add the functionality to send the email.
      */
      await newUserCredential.user.sendEmailVerification();
      this.router.navigateByUrl("confirmation");
      return newUserCredential;
    } catch (error) {
      throw error;
    }
  }

  // mise à jour image et 
  updateProfile(name:string,photoURL:string){
    var user = firebase.auth().currentUser;
    user.updateProfile({
      displayName: name,
      photoURL: photoURL
    }).then(function() {
      // Update successful.
    }).catch(function(error) {
      // An error happened.
    });
    
  }

  // mise à jour du mail
  update_mail(new_user_mail:string){
    var user = firebase.auth().currentUser;

    user.updateEmail(new_user_mail).then(function() {
      //mail de confiration
      user.sendEmailVerification().then(function() {
        //ok
      }).catch(function(error) {
        // An error happened.
      });
      
      // Update successful.
    }).catch(function(error) {
      // An error happened.
    });
  }

  // Changer le mot de pass(1)
  update_password(password:string){
    var user = firebase.auth().currentUser;
    const  newPassword = String(password);

    user.updatePassword(newPassword).then(function() {
      //  successful.
    }).catch(function(error) {
      // An error happened.
    });

  }

  // Changer le mot de pass(2) avec un lien par mail
  changepassword(mail:string){
    var auth = firebase.auth();
   
    auth.sendPasswordResetEmail(mail).then(function() {
      // Email sent.
    }).catch(function(error) {
      // An error happened.
    });

  }

  //SUPRIMER UN UTILISATEUR
  remove_user(){
    var user = firebase.auth().currentUser;

    user.delete().then(function() {
      // User deleted.
    }).catch(function(error) {
      // An error happened.
    });

  }

  // se reauthantifier
  re_auth(){
    var user = firebase.auth().currentUser;
    var credential;

    // Prompt the user to re-provide their sign-in credentials

    user.reauthenticateWithCredential(credential).then(function() {
      // User re-authenticated.
    }).catch(function(error) {
      // An error happened.
    });

  }

}

