import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IvestissementPage } from './ivestissement.page';

const routes: Routes = [
  {
    path: '',
    component: IvestissementPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class IvestissementPageRoutingModule {}
