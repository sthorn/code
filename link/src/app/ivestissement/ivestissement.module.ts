import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { IvestissementPageRoutingModule } from './ivestissement-routing.module';

import { IvestissementPage } from './ivestissement.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    IvestissementPageRoutingModule
  ],
  declarations: [IvestissementPage]
})
export class IvestissementPageModule {}
