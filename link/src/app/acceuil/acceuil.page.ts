import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import firebase from 'firebase/app';

@Component({
  selector: 'app-acceuil',
  templateUrl: './acceuil.page.html',
  styleUrls: ['./acceuil.page.scss'],
})
export class AcceuilPage implements OnInit {
  user:any;
  url_image:string;

  constructor(public router:Router,) {

    //this.goToSetting();
    this.user = firebase.auth().currentUser;
      
    var  photoUrl;

    if (this.user != null) {
      photoUrl = this.user.photoURL;
      //this.usr_name = this.user.displayName;

      photoUrl!=""? this.url_image=photoUrl:this.url_image="assets/icon/person-circle.svg";
    }
   }

  ngOnInit() {
  }
  goToSetting(){
    this.router.navigateByUrl("parametre");
  }

}
