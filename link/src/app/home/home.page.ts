import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, Platform } from '@ionic/angular';
import firebase from 'firebase/app';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from '../authentification/auth.service';




@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  recaptchaVerifier: firebase.auth.RecaptchaVerifier;
  phoneNumber:string;
  unsubscribeBackEvent: any;

  

    constructor
    (public router:Router,
      public platform: Platform,
      private alertCtrl: AlertController,
      private alertCtrl1: AlertController,
      private authservice: AuthService,
      private translate: TranslateService,
    )
    {

    }


  /****************************************
   * go to parameter
   */
  goToSetting(){
    this.router.navigateByUrl("parametre");
  }

  /****************************************
   * Go to connexion page
   */
  goToconnection(){
    this.router.navigateByUrl("connexion");
  }
  /*****************************************
   * Go to singin page
   */
  goToSingin(){
    this.router.navigateByUrl("inscription");
  }

  /**
   * Phone authenification methode
   */
   async phone_aut_process(){
    // choix de la langue 
    firebase.auth().languageCode =this.translate.currentLang;
    //
    this.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container');
    const appVerifier = this.recaptchaVerifier;
    const phoneNumberString = this.phoneNumber;
    firebase.auth().signInWithPhoneNumber(phoneNumberString, appVerifier)
      .then( async (confirmationResult) => {
        // SMS sent. Prompt user to type the code from the message, then sign the
        // user in with confirmationResult.confirm(code).
        let prompt = await this.alertCtrl1.create({
        header:this.translate.instant("ALERT.sms_cofimation.header"),
        inputs: [{ name: 'confirmationCode', placeholder: this.translate.instant("ALERT.sms_cofimation.code") }],
        buttons: [
          { text: this.translate.instant("ALERT.phone_auth.btn_cancel"),
            handler: data => { console.log('Cancel clicked'); }
          },
          { text: this.translate.instant("ALERT.sms_cofimation.envoyer"),
            handler: data => {
              confirmationResult.confirm(data.confirmationCode)
              .then(function (result) {
                // User signed in successfully.
                console.log(result.user);
                // ...
              }).catch(function (error) {
                // User couldn't sign in (bad verification code?)
                // ...
              });
            }
          }
        ]
      });
      await prompt.present();
    })
    .catch(function (error) {
      console.error("SMS not sent", error);
    });
  
  }
  async phone_auth() {
    const alert = await this.alertCtrl.create({
      cssClass: 'my-custom-class',
      header: this.translate.instant("ALERT.phone_auth.header"),
      message:this.translate.instant("ALERT.phone_auth.msg"),
      inputs: [
        {
          cssClass: 'alert_phone',
          name: 'phon_number',
          type: 'tel',
          id: 'name2-id',
          placeholder: '+33 075540265'
        }
      ],
      buttons: [
        {
          text: this.translate.instant("ALERT.phone_auth.btn_cancel"),
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
            
          }
        }, {
          text: this.translate.instant("ALERT.phone_auth.btn_valider"),cssClass: 'validate',
          handler: data => {
            console.log(JSON.stringify(data));
            this.phoneNumber = JSON.stringify(data.phon_number);
            console.log(this.phoneNumber);
            // Vérification robot
            this.phone_aut_process();
          }
        }
      ]
    });

    await alert.present();
  }


  // google auth
  googleAuth(){
    this.authservice.google_singin();
  }
  // facebook auth
  facebookAuth(){
    console.log("int home.page.ts");
    this.authservice.facebook_login();
  }

  //tweter auth
  tweterAuth(){
    this.authservice.tweter_login();
  }

  ionViewDidEnter() {

    this.initializeBackButtonCustomHandler();

  }

  // quite l'aplication quand on fait un back

  initializeBackButtonCustomHandler(): void {

    this.platform.backButton.subscribeWithPriority(999999,  () => {
        if(window.confirm('Do you want to exit the app?'))
        {
          navigator['app'].exitApp();
        }
    });
  }

 
  

}

