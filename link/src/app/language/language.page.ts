import { Component, OnInit } from '@angular/core';
import { LanguageService } from '../service/language.service';
import { AlertController, PopoverController } from '@ionic/angular';
// TranslateService doit etres importé dans les pages qui doivent 
//  être traduites
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-language',
  templateUrl: './language.page.html',
  styleUrls: ['./language.page.scss'],
})
export class LanguagePage implements OnInit {
  languages=[];
  selected = "";
 
  constructor(private popoverCtrl:PopoverController,
    // definition des variables privées
    private languageService:LanguageService,
    private AlertCtclr: AlertController,
    private translate: TranslateService) { }

  ngOnInit() {
   this.languages= this.languageService.getLanguage();
   this.selected = this.languageService.selected;
  }
  /**
   * select methode qui permet de selectionner la valeur de la nouvelle langue
   */
  select(lgn){
    
    this.languageService.setLanguage(lgn);
    this.popoverCtrl.dismiss();
    this.showAlert();
  }
  /**
   * showAlert : message d'alert après modification de d'une langue
   */
  async showAlert(){
    const alert = await this.AlertCtclr.create({
    header:this.translate.instant("ALERT.header"),
    message:this.translate.instant("ALERT.msg"),
    buttons:["OK"]
  });
  await alert.present();
 }

 getLangue(){
   return this.selected;
 }

}
