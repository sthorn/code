import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TontinePage } from './tontine.page';

const routes: Routes = [
  {
    path: '',
    component: TontinePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TontinePageRoutingModule {}
