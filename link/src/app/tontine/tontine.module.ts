import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TontinePageRoutingModule } from './tontine-routing.module';

import { TontinePage } from './tontine.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TontinePageRoutingModule
  ],
  declarations: [TontinePage]
})
export class TontinePageModule {}
