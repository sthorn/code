import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { LanguageService } from './service/language.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {

  
  constructor(private languageService:LanguageService,
    public router: Router
    ) {
    // languages
    this.languageService.initLanguageApp();
    //ajouter un splash sreen 
    this.router.navigateByUrl("splash")
  }
}
