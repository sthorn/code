import { Component, OnInit } from '@angular/core';
import { AuthService } from '../authentification/auth.service';

@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.page.html',
  styleUrls: ['./connexion.page.scss'],
})
export class ConnexionPage implements OnInit {
  email: string;
  password: string;

 

  constructor(public authenticationService: AuthService) {}
  ngOnInit() {
  }

  login() {
    this.authenticationService.mail_login(this.email, this.password);
    this.email = ''; 
    this.password = '';
  }


}
