import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Storage } from '@ionic/storage';

const LNG_KEY = "SELECTED_LANGUAGE0";

@Injectable({
  providedIn: 'root'
})

export class LanguageService {
  selected = '';


  constructor(private translate:TranslateService,
    private storage: Storage) { 
      this.storage.create();
    }

  /**
   * Methode d'initionisation de la langue
   * recupere la langue enreigisté sinon utilise une par defaut(celle du système)
   */
  initLanguageApp(){
    let language = this.translate.getBrowserLang();
    this.translate.setDefaultLang(language);

    this.storage.get(LNG_KEY).then(val =>{
      if(val){
        this.setLanguage(val);
        this.selected = val;
      }
    });
  }

  /**
   * Cette methode parametre la langue gance à la méthode <<.use(lg)>>
   * Mais stock  également la valeur enreigisté dans le mobile grage a storage.set
   * @param lng valeur de la langue (abregé)
   */
  setLanguage(lng){
    this.translate.use(lng);
    this.selected = lng;
    this.storage.set(LNG_KEY,lng);
  }
  
  /**
   * si vous souhaitez rajouter ou retirez une langue tous se passe ici
   * @add pour rajouter creer un fichier lange.json (en.json fr.json) et 
   *      rajouter un dictionnaire comme ci-dessous
   *      @text le nom de la langue
   *      @value abréviation code language iso 
   *      @img icon au format png,svg,jpeg, ... (en gros  une image)
   *   exemple : {text:'English',value:'en',img:'assets/icon/language/etats-unis.png'} pour l'anglais
   * @returns la liste des languages disponible
   */
  getLanguage(){
    return [
      {text:'English',value:'en',img:'assets/icon/language/etats-unis.png'},
      {text:'Francais',value:'fr',img:'assets/icon/language/france.png'},
      {text:'Arabe',value:'ar',img:''},
      {text:'chinoix',value:'zh',img:'assets/icon/language/chine.svg'},
      {text:'Espagne',value:'es',img:'assets/icon/language/espagne.png'},
      {text:'Italien',value:'it',img:'assets/icon/language/italie.png'},
      {text:'Japonais',value:'jp',img:'assets/icon/language/japon.png'},
      {text:'Portugais',value:'pt',img:''},
      {text:'Russe',value:'ru',img:'assets/icon/language/russie.svg'},
      {text:'Latin',value:'la',img:''}
    ];
  }

}

