import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from '../authentification/auth.service';

@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.page.html',
  styleUrls: ['./inscription.page.scss'],
})
export class InscriptionPage implements OnInit {
  user_name:string;
  user_firstname:string;
  user_mail:string;
  user_phone:string;
  user_password:string;
  user_confirm:string;
  politic_:boolean;
  condition_:boolean;
  asyncService: any;

  constructor(private AlertCtclr: AlertController,
    private translate: TranslateService,
    public authenticationService: AuthService,) { 
    }

  ngOnInit() {
  }

  async showAlert(){
    const alert = await this.AlertCtclr.create({
    header:this.translate.instant("ALERT.header"),
    message:this.translate.instant("ALERT.msg"),
    buttons:[this.translate.instant("ALERT.phone_auth.btn_valider")]
  });
  await alert.present();
  }

  /**inscription avec mail */
  async singin_process(){
    
    if(this.condition_){//condition accepter
      if(this.user_name.length >=3 || this.user_firstname.length >=3){//nom valide
        if(this.politic_){//accepter la politique
          /**
           * verification email
           */
           var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
           if(re.test(this.user_mail)) {
             // valide mail
             this.authenticationService.mail_signup(this.user_name, this.user_firstname, this.user_mail,this.user_phone,this.user_password);
           }else{
            const alert = await this.AlertCtclr.create({
              header:this.translate.instant("ALERT.politique.header"),
              message:this.translate.instant("ALERT.politique.msg"),
              buttons:[this.translate.instant("ALERT.phone_auth.btn_valider")]
            });
            await alert.present();
            await alert.onDidDismiss();
            
           }
           
        }
        else{
          const alert = await this.AlertCtclr.create({
            header:this.translate.instant("ALERT.error_mail.header"),
            message:this.translate.instant("ALERT.error_mail.msg"),
            buttons:[this.translate.instant("ALERT.phone_auth.btn_valider")]
          });
          await alert.present();
          await alert.onDidDismiss();
        }
      }
      else{
        const alert = await this.AlertCtclr.create({
          header:this.translate.instant("ALERT.name_error.header"),
          message:this.translate.instant("ALERT.name_error.msg"),
          buttons:[this.translate.instant("ALERT.phone_auth.btn_valider")]
        });
        await alert.present();
        await alert.onDidDismiss();
      }
    }
    else{
      const alert = await this.AlertCtclr.create({
        header:this.translate.instant("ALERT.condition.header"),
        message:this.translate.instant("ALERT.condition.msg"),
        buttons:[this.translate.instant("ALERT.phone_auth.btn_valider")]
      });
      await alert.present();
      await alert.onDidDismiss();
    }
  }



}
