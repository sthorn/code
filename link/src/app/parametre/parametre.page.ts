import { Component, OnInit } from '@angular/core';
import { AlertController, PopoverController } from '@ionic/angular';
import { AuthService } from '../authentification/auth.service';

import { LanguagePage } from '../language/language.page';

import firebase from 'firebase/app';
import { Router } from '@angular/router';

@Component({
  selector: 'app-parametre',
  templateUrl: './parametre.page.html',
  styleUrls: ['./parametre.page.scss'],
})

export class ParametrePage implements OnInit {
  user: firebase.User;
  url_image:string;
  usr_name:string;

  constructor(
    private popoverController: PopoverController,
    private AlertCtclr: AlertController,
    private authservice: AuthService,
    public router:Router
    )
    {
      this.user = firebase.auth().currentUser;
      
      var  photoUrl;

      if (this.user != null) {
        photoUrl = this.user.photoURL;
        this.usr_name = this.user.displayName;

        photoUrl!=""? this.url_image=photoUrl:this.url_image="assets/icon/person-circle.svg";
      }

      
    }

  ngOnInit() {
  }
  
  /**
   * 
   * @param ev event
   * methode asyncron genérant un popup pour changer des langue
   */
  async languepopover(ev: any) {
    const popover = await this.popoverController.create({
        component: LanguagePage,
        event: ev,
      });
    await popover.present();
  }
  //logout
  logout(){
    this.authservice.logout();
  }
}
